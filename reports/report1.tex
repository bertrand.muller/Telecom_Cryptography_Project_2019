\documentclass{tnreport1}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{Système de contrôle d'accès par badge} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{lipsum}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}
\usepackage{enumitem}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage

\chapter{Introduction}

En tant que spécialiste des systèmes de contrôle d'accès sans fil, notre entreprise Supersur souhaite présenter un tout nouveau système basé sur l'utilisation de badges. Ce rapport vise donc à détailler notre solution et à indiquer en quoi il répond à vos attentes en terme de sécurité, de flexibilité et de praticité.  Afin de fixer nos attentes réciproques, ce rapport offre un point de vue technique sur son implantation en vue de répondre à vos besoins. 

Afin de mettre l'accent sur la plus-value de ce produit, nous allons analyser vos besoins, présenter une architecture globale adaptée à votre entreprise, détailler les outils cryptographiques utilisés et montrer en quoi ils sécurisent vos locaux. Bien évidemment, ceci est complété par une analyse financière quant au coût de la solution et une analyse des menaces en termes de cryptanalyse ou d'attaque. 

Avant de débuter l'analyse des besoins, il est important de cerner les enjeux quant à la mise en place d'un système de contrôle d'accès sans fil. Depuis la fin des années 1990, la communication sans fil s'est très vite propagée par l'apparition des téléphones portables. Depuis lors, les entreprises cherchent à sécuriser leurs systèmes en utilisant elles-mêmes des technologies sans fil. Malgré ces importants changements, un faible pourcentage des systèmes est 100\% sans fil. C'est pourquoi notre entreprise s'est spécialisée dans la création de nouveaux systèmes plus flexibles, plus faciles à configurer et moins coûteux. En effet, la popularité de tels systèmes s'explique par l'avantage de ne pas être perturbants pour les infrastructures en place puisqu'ils ne nécessitent ni alimentation ni câblage. De plus, ils sont désormais essentiels pour la sécurisation des entreprises et plus particulièrement des locaux pour garantir le secret industriel ou intellectuel. 

Ce rapport vise donc à répondre à vos besoins en terme de sécurisation mais aussi à montrer en quoi notre solution s'inscrit dans une nouvelle ère puisqu'il sera amené à perdurer. 

\chapter{Analyse des besoins}

Suite à nos précédentes rencontres et nos différents échanges, nous avons pris l'initiative de réaliser une analyse complète de vos besoins. Nous souhaitons nous adapter pleinement à vos attentes en les retranscrivant pour validation. Afin de vous offrir la meilleure solution, nous avons fait le choix de présenter chacune des finalités de ce projet. 

\section{Sécurisation des locaux}

Dans le cadre de ce projet, nous avons été sollicités par votre entreprise pour la mise en place d'un système de contrôle d'accès physique et sécurisé. Dans ce contexte, nous sommes chargés de fournir un système permettant la sécurisation des locaux et donc de garantir que l'accès d'une pièce ou d'un bâtiment est accordée aux seules personnes habilitées. En dehors de l'aspect fonctionnel, cela signifie que différents types de bâtiments, bureaux ou salles doivent être pris en compte lors de l'installation du système. En effet, le niveau de sécurisation n'est pas identique entre un bureau et une salle réservée à la direction de l'entreprise (considérée comme étant de haute sécurité). Nous reviendrons plus tard sur ces niveaux de sécurisation lors de la présentation de l'architecture globale dans la partie \ref{ch:Architecture_Globale}.

Afin de mettre en place une telle architecture, il est nécessaire d'étudier les locaux sur plan. Grâce aux éléments fournis, nous avons comptabilisé 68 pièces pour un seul et même bâtiment. Elles se décomposent de la manière suivante : 

\begin{itemize}[noitemsep,topsep=0pt]
	\item \textbf{15} bureaux par étage
	\item \textbf{5} bureaux pour le comité d'entreprise
	\item \textbf{5} salles de réunion
	\item \textbf{3} salles serveur
	\item \textbf{3} salles de pause
	\item \textbf{2} entrées
	\item \textbf{2} salles d'archives
	\item \textbf{1} salle de réception
	\item \textbf{1} bibliothèque
	\item \textbf{1} centre technique
\end{itemize}

Par ailleurs, un besoin supplémentaire a été porté à notre connaissance dans le cadre de ce projet. Afin de garantir une sécurité supplémentaire, il a été souhaité que les salles serveurs soient munies au minimum d'un double système de contrôle. Ces deux systèmes doivent être différents afin de prévenir toute faille et d'éviter des vols de données ou de matériel. 

\section{Accès par badge}

Afin d'assurer une sécurité multi-niveau, votre structure privilégie la mise en place d'un accès par badge. Cependant, il est nécessaire d'expliciter les raisons pour lesquelles ce système a été favorisé. Un badge sera donné à chaque salarié de votre entreprise ou à tout visiteur pour leur permettre de s'identifier et de leur autoriser l'accès aux différentes pièces listées précédemment. 

Les lecteurs de badge sont placés à l'entrée de chacune des salles et sont en attente de présentation d'un badge. Lorsque la distance entre un badge et le lecteur est suffisante, le système est dans la capacité d'accorder ou non l'accès selon les privilèges accordés au détenteur du badge. Les lecteurs ont la capacité d'être modifiés en temps réel et de proposer une traçabilité des entrées et des sorties. L'historisation des données permet de rechercher les incidents (comme l'usurpation d'identité) qui peuvent se produire. De plus, cela s'inscrit dans votre volonté de protéger les informations personnelles et sensibles contre de possibles intrusions. Des documents sur support physique ou numérique sont stockés à différents endroits dans les locaux (salles serveur, archives, ...). Ces lieux sont donc considérés comme critiques. 

Pour répondre à vos souhaits quant à la mise en place d'un système simple, le lecteur de badge semble être la meilleure solution de par sa simplicité d'utilisation et sa gestion fine des accès (géographiquement ou utilisateur/utilisateur).  

\section{Niveaux d'accès}

Pour gérer les différents niveaux de sécurité, il est nécessaire de lister les catégories d'employés qui seront amenés à interagir avec le système. D'après les données que nous avons pu rassembler, 200 personnes travaillent sur le site avec un nombre de salariés moyen par étage égal à 40. Les catégories de personnel ont été rassemblées dans la liste ci-dessous, avec un niveau de privilège graduel. 

\begin{center}
	\begin{tabular}{c|p{12cm}}
		\centering
		\textbf{Personnel} & \textbf{Accès accordés} \\
		\hline
		\multirow{3}{*}{\textit{\makecell{Intervenants\\ extérieurs}}} & Ils auront accès à certaines pièces communes que sont l'accueil et les salles de pause. On estime qu'ils devront être accompagnés pour tout autre accès.\\
		\hline
		\multirow{3}{*}{\textit{Salariés}} & Par l'intermédiaire de leur badge, l'ensemble des salariés auront accès aux lieux communs d'accueil, de pause, de réunion mais également au bureau dans lequel ils travaillent. \\
		\hline
		\multirow{1}{*}{\textit{\makecell{Agents\\ d'entretien}}} & Ils auront accès aux lieux communs mais aussi à l'ensemble des bureaux occupés par les autres salariés de l'entreprise.\\
		\hline
		\multirow{2}{*}{\textit{\makecell{Agents\\ de sécurité}}} & Ils auront accès à l'ensemble des pièces pour veiller à la sécurité des locaux.\\
		\hline
		\multirow{2}{*}{\textit{\makecell{Administrateurs\\ du système d'accès}}} & Ils auront accès aux mêmes salles que les autres salariés mais auront l'autorisation de modifier les accès pour tel ou tel employé. \\
		\hline
		\multirow{2}{*}{\textit{\makecell{Directeur\\ de centre}}} & Il aura accès à toutes les pièces sauf à la salle des serveurs puisqu'elle nécessite des autorisations particulières.
	\end{tabular}
\end{center}

Bien évidemment, ces niveaux d'accès sont modulables. Cette proposition est basée sur les observations que nous avons pu effectuer dans votre entreprise et au travers de notre expertise dans ce domaine. Une interface sera mise à disposition pour effectuer tout changement souhaité. 

\section{Disponiblité}

D'un point de vue non fonctionnel, le système devra être opérationnel 99\% du temps. Le nombre de pannes devra donc être limité. Des tests en amont et en aval de l'installation du système de sécurité seront effectués pour s'assurer de respecter la contrainte de disponibilité. Par ailleurs, les scénarios exceptionnels tels que les inondations, incendies, coupures de courant ou de réseaux doivent être pris en compte au moment de l'installation. Le système devra être en mesure de fonctionner dans un mode dégradé. 

Concernant l'utilisateur, un temps de réponse d'un lecteur de badge est jugé acceptable s'il est inférieur à 50ms. A l'instar de la contrainte précédente, des tests de performances seront mis en place pour prouver la capacité du système à répondre en un temps convenable. 

\section{Sûreté}

Le système devra bien entendu respecter les normes de sécurité relatives à sa mise en place. En d'autres termes, cela signifie que toutes les données qui circulent sur le réseau doivent être sécurisées et chiffrées. Pour cela, une ou des méthodes de cryptographie doivent être utilisées pour se prémunir des éventuelles attaques internes ou externes à l'entreprise. 

La sécurité doit être prise en compte de bout en bout avec des choix pertinents concernant les méthodes cryptographiques, les protocoles, les installations matériel et les badges utilisés. 

\section{Usage}

D'autre part, votre entreprise souhaite qu'un protocole soit suivi lorsqu'un changement de droit d'accès doit être effectué. Deux catégories de personnes seront alors amenées à faire partie de ce processus : la direction et les administrateurs du système. La direction pourra alors recevoir des demandes de modification par l'intermédiaire du système de dépôt de ticket de l'entreprise. Lorsque cette demande aura été validée, la personne se verra accorder ou retirer un accès. Pour cela, les administrateurs seront chargés de prendre en compte cette demande et d'effectuer la modification en conséquence.

Une interface de modification est donc souhaitée pour permettre aux administrateurs de réaliser ces opérations. Les fonctionnalités proposées par le système pour ce besoin seront présentées dans la suite de ce document. 

\section{Coût financier}

Pour finaliser cette partie sur l'analyse des besoins, quelques données supplémentaires sont à prendre en compte pour la mise en place du système à contrôle d'accès. En effet, un badge doit coûter moins de 10€ à l'unité et un lecteur de badge moins de 60€. Les frais de maintenance, de formation du personnel et de mise en place du matériel pourront être négociés après validation de la solution exposée.

\chapter{Architecture globale}

\label{ch:Architecture_Globale}
Dans ce chapitre, on souhaite faire une présentation de l'architecture globale du système. Pour cela, nous avons fait le choix de l'aborder en deux temps : les installations physiques devant être mises en place en fonction des types de salle et le cryptosystème permettant de vérifier les accès. La première partie se veut informative tandis que la deuxième est plus technique. 

\section{Installations physiques}

\subsection{Badges}

Pour l'ensemble des salariés déjà présents dans l'entreprise, ils se verront remettre un badge unique et personnel. Pour les nouveaux embauchés, un nouveau badge devra été produit et enregistré dans le système. Ces badges offrent la possibilité à chacun des employés d'accéder à différentes salles en fonction du poste occupé. Un badge contiendra un identifiant unique et propre à chacun des salariés. Ce numéro d'identification peut être connu de tous : il ne fait pas partie du cryptosystème à part entière. Il permet notamment d'identifier un individu pour la mise à jour des accès. 

En revanche, un numéro supplémentaire, également unique, devra rester secret et connu de la personne détentrice du badge. Ce numéro sera composé de neuf chiffres et pourra être utilisé pour un niveau de sécurité supérieur en fonction des types de salles décrits dans la suite de ce rapport. 

\subsection{Salles}  

\subsubsection*{Entrées}

Tout d'abord, on souhaite préciser les modalités d'accès au bâtiment. Pour ouvrir les portes d'entrée, chaque salarié devra utiliser son badge et le mettre à bonne distance du lecteur. Une fois que le badge aura été reconnu, le système décide d'accorder l'accès ou non à la personne concernée. Pour ne pas entraver l'accès au bâtiment pendant les heures de pointe, le système devra réagir rapidement.

Pour sortir par ces mêmes portes, il sera également nécessaire de badger afin d'obtenir une historisation précise des entrées et des sorties. 

\subsubsection*{Bureaux}

Pour les 50 bureaux à sécuriser, nous vous proposons de les sécuriser grâce à une lecture de badge. En effet, lorsqu'un salarié souhaite accéder à son espace de travail, il doit être amené à le présenter devant le lecteur prévu à cet effet. L'accès sera alors refusé si le badge est non reconnu, si la personne n'est pas autorisée à y accéder ou s'il est utilisé en dehors des horaires de travail. 

Cependant, d'autres personnes pourront y accéder indépendamment de l'horaire ou de la personne qui s'authentifie. En effet, les agents de sécurité seront autorisés à accéder à tous les bureaux de n'importe quel étage pour s'assurer que l'ensemble des employés ont bien quitté les lieux. Il en est de même pour les agents d'entretien qui doivent pouvoir nettoyer les locaux une fois que les autres personnes sont parties. Bien sûr, le directeur de centre et les intervenants extérieurs n'auront pas l'autorisation de pénétrer dans ces lieux sans le consentement des salariés. 

Par ailleurs, le salarié aura la possibilité de déposer un ticket auprès de la direction ou des administrateurs du système pour renforcer la sécurité d'accès à leur bureau. Si cela est souhaité, le code unique et secret sera utilisé comme deuxième niveau d'authentification après l'identification par badge. Une fonctionnalité doit donc être prévue au niveau des interfaces d'administration. 

Il est nécessaire de prévoir 50 lecteurs de badges à capacité de calcul limité. En effet, malgré une sécurité importante, il n'est pas nécessaire d'utiliser des appareils avec beaucoup de fonctionnalités puisque l'on souhaite simplement lire un badge ou entrer un code. 

\subsubsection*{Salles de réunion}

A l'instar des bureaux, les salles de réunion pourront uniquement être accessibles par l'utilisation des badges. En revanche, pour cette fois, l'accès sera autorisé pendant un certain temps. Le système de réservation de ces salles (\textit{Outlook Meeting}) permettra de déterminer les périodes pendant lesquelles les personnes conviées à une réunion auront les accès pour l'ouverture. 

Afin d'éviter les erreurs humaines et les dépôts de tickets auprès des administrateurs, la durée d'accès autorisé pourra être légèrement étendue avant et après la réunion. Ceci correspond donc à une nouvelle fonctionnalité de l'interface d'administration afin de la changer voire même de la retirer. Les administrateurs pourront également faire le choix d'utiliser la double authentification si cela est nécessaire. 

Comme pour les bureaux, ces salles ne seront accessibles que pendant les horaires de travail et par les agents de sécurité et d'entretien à n'importe quel moment. Toutefois, pour effectuer tous les contrôles décrits précédemment, il est nécessaire d'installer des machines plus évoluées pour confirmer la réunion, l'annuler, vérifier les badges et les horaires.

\subsubsection*{Salles de pauses}

Les salles de pauses ont un système similaire mais toutefois différent au niveau des contrôles effectués. Le contrôle d'accès pourra en effet être désactivé la majorité du temps. Cependant, il reste important de les rendre inaccessibles lors d'un contrôle technique des machines ou pour permettre au personnel sur place (en dehors des heures de travail) de pouvoir y accéder. L'interface d'administration permet de régler l'ensemble de ces paramètres et surtout de sélectionner les dates pour lesquelles l'accès doit être contrôlé. 

\subsubsection*{Salles critiques ou de haute sécurité}

Pour ces derniers types de salles, la sécurité est encore plus importante. En effet, elles regroupent les salles serveurs, d'archives et le centre technique. Elles contiennent donc des informations sensibles ou offrent le moyen de réaliser des actions portant atteintes aux personnes. Dans ce cas, il est primordial de garantir une triple sécurité pour s'assurer que les personnes qui y pénètrent y sont bien autorisées. 

Comme pour les autres salles, le premier niveau consiste à s'authentifier par badge et le second niveau à rentrer son code secret à neuf chiffres. Cependant, pour ces salles dites de haute sécurité, il devra y avoir au moins la présence de deux personnes habilitées. Les deux devront donc passer leur badge et entrer leur code successivement. Cela oblige l'entreprise à faire en sorte que deux personnes habilitées soient toujours présentes sur site.

\subsubsection*{Salle de réception et bibliothèque}

Enfin, ces deux dernières salles auront un système de sécurité pour garantir la non présence d'employés dans ces lieux en dehors des heures d'activité. L'agent de sécurité sera le seul habilité à y accéder pour vérifier l'occupation ou non des lieux. Le reste du temps ces salles seront ouvertes sans contrôle d'accès par défaut. Bien évidemment, tout administrateur aura la possibilité de modifier ces conditions d'accès depuis l'interface qui lui sera destinée. 

\subsubsection*{Pour toutes les salles...}

Pour toutes les salles, les accès pourront être modifiés depuis l'interface de gestion et par les personnes habilitées. De plus, afin de parer tout imprévu humain ou du système, un administrateur pourra donner des droits d'accès à n'importe quelle autre personne, et ce pour n'importe quel type de salle. Ces changements critiques seront vérifiés par un autre moyen d'authentification détaillé dans la partie technique qui suit. 

\section{Fonctionnement}

\subsection{Scénario type}

Pour comprendre le système global, il est nécessaire de présenter la manière dont un lecteur de badge doit être utilisé :

\textbf{1.} Tout d'abord, l'employé doit présenter son badge au niveau du lecteur. Si le badge peut être lu, on passe à l'étape suivante. Dans le cas contraire, la personne retente de passer son badge si elle pense avoir les accès nécessaires. L'accès est totalement refusé si la personne n'a pas les droits d'accéder à la pièce souhaitée. 

\textbf{2.} Les informations lues depuis le badge (son identifiant unique) sont transmises au serveur d'authentification présent dans les locaux de l'entreprise. 

\textbf{3.} Le serveur vérifie que les informations envoyées existent dans la base de données. Si ce n'est pas le cas, une erreur est renvoyée vers le lecteur de badge. Ceci se traduit par l'affichage d'un message d'erreur à destination de l'employé. Si le badge est reconnu, le serveur se charge de vérifier que les accès accordés au détenteur du badge sont valides pour accéder à la pièce.

\textbf{4.} Si l'accès n'a pas été accordé à la personne concernée, le lecteur de badge reçoit un message d'erreur de la part du serveur. Ce dernier est alors affiché au niveau de l'écran du lecteur. Dans le cas contraire, un message de type "Accès accordé" est renvoyé à la personne.  

\textbf{5.} S'il s'agit d'une pièce de haute sécurité (critique), le système demandera alors à la personne de rentrer son code secret pour s'authentifier. En cas de succès, la pièce est libre d'accès. En revanche, si le code est erroné, la personne possède trois autres tentatives pour entrer le bon code. En cas d'échec, après ces trois tentatives la personne sera dans l'obligation de repasser son badge et donc de reprendre à l'étape \textbf{1}.

\textbf{6.} Enfin, si l'accès est définitivement accordé, la personne pourra accéder aux lieux voulus en ouvrant la porte. Tout employé dont son accès n'est pas valide ne pourra pas ouvrir la porte, protégée par un système de fermeture.

\textbf{7.} Lorsque la porte s'est refermée, dans le cas d'une salle de réunion par exemple, les autres personnes devront effectuer l'ensemble de ces opérations pour également être en mesure de rentrer. Toutefois, la porte peut être ouverte de l'intérieur par les personnes déjà présentes. 

L'annexe \ref{annexe:diagramme} présente un diagramme résumant ce scénario type d'ouverture de porte. 

\subsection{Serveur d'authentification}

En vue de connaître les accès autorisés pour chacun des employés, un serveur devra être mis en place au sein de l'entreprise. Il stockera en base de données l'ensemble des informations relatives au badge de chacun des salariés. Les données sauvegardées seront alors les suivantes :

\begin{center}
	\begin{tabular}{|c|c|c|c|c|c|c|}
		\hline
		& \textbf{\makecell{Identifiant unique\\du badge}} & \textbf{\makecell{Prénom\\du détenteur}} & \textbf{\makecell{Nom\\du détenteur}} & \textbf{\makecell{Niveau\\d'accès}} & \textbf{\makecell{Code\\secret}} \\
		\hline
		\textit{Exemple} & 55458 & Paul & Marius & 2 & 6153A6...E47  \\
		\hline
	\end{tabular}
\end{center}
\begin{center}
	\begin{tabular}{|c|c|c|c|}
		\hline
		(suite) & \textbf{\makecell{Date de\\mise en service}} & \textbf{\makecell{Date\\d'expiration}} & \textbf{\makecell{Accès\\supplémentaires}}\\
		\hline
		\textit{Exemple} & 14/02/2019 & 12/12/2019 & 8;7;1 \\
		\hline
	\end{tabular}
\end{center}

Bien sûr, l'ensemble de ces données sont renseignées pour tout nouvel embauché. Pour précision, le code secret sera stocké sous la forme d'une valeur hachée afin qu'éviter qu'il ne soit directement en clair dans le système de gestion de bases de données. En ce qui concerne les accès supplémentaires, la chaîne de caractères \textbf{8;7;1} se lit : "l'employé aura accès aux pièces avec les lecteurs de badges 8, 7 et 1 en plus de son niveau d'accès par défaut".

Par ailleurs, le serveur dédié à la gestion des accès ne sera accessible qu'au sein de réseau de l'entreprise pour parer à toute attaque ou intrusion depuis l'extérieur. Cette option permet de fortement améliorer la sécurité du système global puisque les demandes d'accès seront toujours transmises par le réseau local. 

\subsection{Système d'administration}

Comme évoqué lors de l'analyse des besoins, il est primordial de mettre en place un outil de gestion des accès pour les administrateurs. Nous avons donc listé les différents besoins auxquels le système répondra : 

\begin{itemize}
	\item \textbf{Ajout d'un nouveau badge}. Cette fonctionnalité consiste à créer un nouveau badge avec un identifiant unique et à renseigner les informations relatives à son détenteur. La date de mise en service est automatiquement renseignée. En revanche, l'administrateur a la possibilité de renseigner une date d'expiration du badge. Par défaut, aucune date n'est précisée et le badge a une durée de validité infinie. Par ailleurs, le code unique du badge doit être envoyé à la personne concernée. Pour cela, elle recevra un mail avec un lien à usage unique lui indiquant son code secret. De cette manière, aucune autre personne n'a connaissance de ce code (même l'administrateur).
	
	\medskip 
	
	\item \textbf{Suppression d'un badge}. Lors du départ d'un employé, un administrateur peut supprimer de la base de données le badge correspondant. La suppression peut également se faire pour plusieurs badges à la fois lorsque de nouveaux badges sont mis en circulation. Dans le cadre es changement de badges, les suppressions seront toutes notifiées par mail aux employés concernés.  
	
	\medskip
	
	\item \textbf{Modification des données personnelles propres à un badge}. L'administrateur peut modifier les informations personnelles d'une personne, suite à une demande spéciale de celle-ci (par un dépôt de ticket). L'employé sera notifié par mail de ce changement. 
	
	\medskip
	
	\item \textbf{Modification de la date d'expiration d'un badge}. A l'image des informations personnelles du détenteur, l'administrateur peut décider de modifier la date d'expiration du badge. Un mail sera alors automatiquement envoyé à la personne concernée par ce changement. 
	
	\medskip
	
	\item \textbf{Modification des accès}. Bien évidemment, un administrateur a le pouvoir de modifier les accès accordés à une catégorie d'employés ou à un employé en particulier. Tous les employés concernés par un tel changement seront notifiés par mail.  
	
	\medskip
	
	\item \textbf{Verrouillage de tout accès à une salle donnée}. Pour une raison particulière (maintenance de salle, contrôles techniques, réservations exceptionnelles...), un administrateur peut bloquer l'accès à une salle donnée pour une durée spécifiée par ses soins. 
	
	\medskip
	
	\item \textbf{(Dés-)Activation de la double sécurité}. Pour une période donnée ou infinie, l'administrateur a la possibilité d'indiquer si une salle doit appliquer la double authentification. Dès que cette opération sera validée, le lecteur de badge activera ou non cette fonctionnalité. 
	
	\medskip
	
	\item \textbf{Modification du temps de dépassement pour les salles de réunion}. Pour toutes les salles de réunion, le temps de dépassement est de 30min par défaut. Ceci signifie que si les personnes arrivent jusqu'à une demi-heure en retard, ils pourront accéder à la salle normalement. L'administrateur aura donc la possibilité de modifier ce temps de débordement. 
	
	\medskip 
	
	\item \textbf{Ajout/Suppression d'une salle}. L'interface de gestion proposera une fonctionnalité permettant d'ajouter ou supprimer une salle du système. Ceci peut être utile en cas d'agrandissement du site.
	
	\medskip
	
	\item \textbf{Ajout d'un administrateur}. Tout administrateur peut ajouter un nouvel administrateur pour gérer les accès. Cette fonctionnalité est utile en cas d'un changement de poste d'un administrateur actuel ou après une nouvelle embauche. 
\end{itemize}

Les tâches d'administration seront déléguées à des personnes de confiance qui auront un accès à un logiciel sur leur poste de travail. Ce logiciel offrira donc l'interface voulue et permettra de dialoguer avec le serveur d'authentification et sa base de données. Il proposera également un historique des lectures de badges effectuées dans tout le bâtiment. L'administrateur aura la possibilité de filtrer les journaux d'historisation afin de retrouver plus facilement les informations qui lui sont nécessaires. Les bureaux où se situeront ces postes de travail seront considérés comme des salles de haute sécurité.

\section{Cryptosystème}

\subsection{Technologie envisagée}

Nous envisageons d'utiliser notre solution \textbf{SuperSur 2.0}, certifiée par l'\textit{Agence Nationale de la Sécurité des Systèmes d'Information} (ANSSI). Elle repose sur des lecteurs RFID dernière génération respectant les normes de sécurité d'un point de vue technique et de l'utilisation client, dans le but de garantir un niveau de sécurité maximal. 

\subsection{Badges}

En vue de garantir un niveau minimum de sécurité, les badges devront respecter les \textit{Critères Communs} qui sont un ensemble de normes internationales pour la sécurité des systèmes. Ces règles permettent de spécifier et de détailler les niveaux de sécurité attendus en fonction de la sécurité souhaitée. Dans le cadre de ce projet, les lecteurs ne reconnaîtront que des badges avec un \textit{Evaluation Assurance Level} (EAL) au minimum de 4 (EAL4+). Le niveau \textbf{EAL4+} est généralement appliqué pour des applications civiles à sécurité renforcée. Il garantit que le système a été conçu, testé et vérifié selon un minimum de critères de confiance.

Par ailleurs, pour garantir l'efficacité du système cryptographique, il est primordial que les badges ne contiennent que le numéro d'identification unique et l'identité du détenteur sur l'un de ses côtés. En aucun cas, les badges doivent faire apparaître le code secret nécessaire pour l'authentification d'accès aux différentes salles. 

Enfin, les données doivent être protégées par un canal sécurisé lors d'une communication entre un lecteur et un badge. Pour cela, les badges doivent être utilisés en mode sans contact par le protocole \textit{Identification-Authentification-Signature European-Citizen-Card} (\textbf{IAS-ECC}). IAS-ECC est la transcription de la norme européenne ECC en France (IAS). Ce protocole fournit notamment des méthodes cryptographiques pour l'identification et l'authentification. Il correspond donc parfaitement aux besoins spécifiés dans ce projet. 

\subsection{Lecteurs de badges}

Afin de garantir une sécurité du badge au serveur d'authentification, nos lecteurs s'appuient sur le protocole de communication \textit{STid Secure Common Protocol} (\textbf{SSCP}) . Cette sécurité est garantie par le chiffrement des données grâce à l'algorithme de chiffrement symétrique \textit{Advanced Encryption Standard} (\textbf{AES}) 128 bits. Ce protocole cryptographique est également préconisé par l'ANSSI et renforce donc la sécurité de nos systèmes. De plus, il permet également d'authentifier les données issues d'un badge auprès du contrôleur (serveur). 

Puisque nos lecteurs répondent aux normes et standards internationaux et reposent sur le protocole SSCP, on peut attester de la pérennité et de la fiabilité de notre système. De plus, on répond aux contraintes de temps de réponse exposées lors de l'analyse des besoins. En effet, la fréquence utilisée par les lecteurs est de \textbf{13.56 MHz}, ce qui permet de valider ou non un accès en moins de 50ms. Ceci prend bien évidemment en compte le temps de transmission sur le réseau local, le temps de traitement par le serveur et le temps de transmission de la réponse. 

Des verrous devront également être mis en place pour chacun des lecteurs afin d'empêcher la destruction d'un lecteur ou la surcharge électrique. Ils ont pour mission d'alerter le serveur lorsqu'un tel événement se produit. Les administrateurs en seront automatiquement informés via un système d'alerte.

\subsection{Serveur d'authentification}

Le rôle principal du serveur d'authentification est de recevoir les demandes d'accès à certaines salles et à répondre à ces dernières. Pour cela, les lecteurs, par l'intermédiaire de leur tête de lecture, envoient au serveur leur demande via le protocole sécurisé SSCP. Après réception, le serveur est chargé de chercher dans la base de données le niveau d'accès nécessaire pour ouvrir la salle correspondante et de vérifier si le badge utilisé permet de l'ouvrir. 

Comme évoqué précédemment, une historisation sera mise en place pour permettre de vérifier les entrées-sorties, les nombres d'essais infructueux, ... Ainsi, à chaque fois qu'une demande sera effectuée, la demande sera sauvegardée ainsi que tout le processus de vérification et la réponse renvoyée par le serveur. De cette manière, les administrateurs pourront identifier d'éventuels problèmes d'authentification. 

De plus, les codes secrets des badges seront stockés grâce à l'algorithme de hachage \textbf{SHA-256} pour éviter de les laisser en clair. Nous avons décidé de choisir cette fontion car elle correspond à un standard du \textit{National Institute of Standards and Technology} (\textbf{NIST}) aux États-Unis.

Bien évidemment, le serveur sera davantage sécurisé par la mise en place d'un pare-feu, d'un système d'authentification et connecté au réseau local pour éviter toute intrusion depuis l'extérieur. 

\chapter{Analyse des risques}
Comme vous le savez, aucun système ne peut être fiable à 100\%. De ce fait, nous avons analysé les différents risques qui pourraient se produire. Pour chaque risque, nous vous apportons également une solution envisageable.

\section{Coupure électrique}
Nous installerons un groupe électrogène. En cas de coupure de courant, celui-ci prendra le relais après quelques secondes. De ce fait, le système de sécurité continuera de fonctionner durant la panne.

\section{Vol ou perte de badge}
Si un employé perd son badge ou si une personne lui vole, il pourra demander aux administrateurs de désactiver son badge. Grâce à cette fonctionnalité du système d'administration, la personne possédant ce badge ne pourra pas accéder au bâtiment. Si jamais ce badge est utilisé pour l'une des salles, les administrateurs en seront aussitôt informés par un système d'alerte via l'interface de gestion. 

\section{Panne du serveur d'authentification}
Le serveur principal sera répliqué sur un deuxième serveur. De cette manière, si un dysfonctionnement survient, le deuxième serveur sera en capacité de prendre le relais et d'assurer le maintien du système de sécurité. Les données seront répliquées tous les soirs à minuit.

\section{Erreur de manipulation}
En cas d'erreur de manipulation, grâce au système de restauration et de redondance mis en place dans la partie précédente, il sera assez facile de restaurer le système à une version antérieure. Ainsi, en cas d'erreur de manipulation ou de configuration d'un administrateur, il pourra annuler ses modifications.

\section{Copie de badge}
Si un utilisateur malveillant copie un badge sans que son détenteur s'en aperçoive, celui-ci aura un accès au bâtiment. Cependant, l'accès sera limité. En effet, si l'utilisateur ne connaît pas le code du badge, il ne pourra accéder qu'aux salles n'ayant pas le système de double authentification.

\section{Attaque informatique}
Le système de sécurité est isolé. Il n'est accessible qu'en local depuis certains ordinateurs. Les postes ayant accès au système devront être sécurisés (antivirus). Cependant, si une cyberattaque survient, vous devrez immédiatement nous en informer. Suite à cela, nos équipes viendront isoler le système et chercheront les éventuelles failles de sécurité. Une fois trouvées, nos équipes sécuriseront votre système et en effectueront une restauration.

\section{Entrée en force}
Si une serrure est ouverte sans qu'un badge n'ait été scanné, une alarme centrale sera déclenchée. De ce fait, cela dissuadera les éventuelles personnes souhaitant entrer en force à le faire.

\section{Cas de force majeure}
En cas de force majeure (incendie, inondation, ...), nous prévoirons une fonctionnalité qui désactivera le système (ouverture de toutes les portes). Grâce à cela, votre personnel pourra évacuer en toute sécurité et les services de secours pourront intervenir sans difficulté.

\chapter{Analyse financière}
Suite à l'analyse de vos besoins, nous vous avons proposé une solution complète pour sécuriser les différents accès de votre bâtiment. Voici les différents coûts pour cette prestation : \\
\begin{center}
	\begin{tabular}{lrrr}
		\textbf{Nom de l'article} & \textbf{Prix H.T.} & \textbf{Quantité} & \textbf{Prix total H.T.} \\
		\hline
		Badge RFID & 8,90€ & 250 & 2 225,00€\\
		Lecteur RFID avec clavier numérique & 54,90€ & 136 & 7 466,40€\\
		Serrure électrique sécurisée & 299,00€ & 68 & 20 332,00€ \\
		Tourniquet tripode & 2 568,90€ & 3 & 7 706,70€\\
		Portillon PMR & 5 627,20€ & 1 &  5 627,20€\\
		Licence du système d'administration & 2 000,00€ & 1 & 2 000,00€\\
		Maintenance du système d'administration & 250,00€ & 10 ans & 2 500,00€\\
		Support client & 200,00€ & 10 ans & 2 000,00€\\
		Serveur d'authentification & 849,90€ & 2 & 1 699,80€\\
		Frais d'installation et de configuration & 29,50€ & 180 h & 5 310,00€\\ 
		Câble électrique (100 m) & 58,90€ & 10 & 589,00€\\
		Câble Ethernet & 9,99€ & 250 & 2 497,50€\\
		Alarme sonore & 16,00€ & 68 & 1 088,00€\\
		Groupe électrogène & 6 495,00€ & 1 & 6 495,00€\\
		&&&\\
		&& \textbf{Total H.T.} & 67 545,60€\\
		&& \textbf{TVA (20\%)} & 11 547,12€\\
		&& \textbf{TVA (10\%)} & 981,00€\\
		&& \textbf{Total T.T.C.} & \textbf{80 073,72€}\\
	\end{tabular}
\end{center}

\clearpage

\chapter{Conclusion}

Pour conclure quant à ce projet, nous proposons de mettre en place une solution de contrôle d'accès par badge combinée à une composition de code pour les salles de haute sécurité. Globalement, cette solution est peu complexe à mettre en place. Elle nécessite simplement de veiller à la bonne installation du matériel et d'être testée pendant les premières semaines d'utilisation. Cependant, afin de garantir une sécurité, une fiabilité et une simplicité de la solution, différentes méthodes cryptographiques et protocoles de sécurité seront adoptés quant à notre expertise. 

Comme exprimé dans l'analyse des besoins, une interface de gestion des accès sera mise en place pour permettre aux administrateurs (désignés par l'entreprise) de configurer le système. Un ensemble de fonctionnalités ont été prévues ainsi qu'un certain nombre de situations exceptionnelles. Des fonctionnalités supplémentaires pourront être demandées auprès de notre structure, ce qui fera l'objet d'une prestation complémentaire.  

Enfin, une dernier point reste à prévoir pour votre entreprise. En effet, vous devrez veiller à former votre personnel quant à cette solution et à mettre en place des processus de maintenabilité, de sécurité et d'information. 

\appendix
\part*{Annexes}
\addcontentsline{toc}{part}{Annexes}

\chapter{Diagramme - Scénario d'ouverture de porte}
\label{annexe:diagramme}
\begin{center}
	\vspace*{-0.8in}
	\includegraphics[scale=0.74]{figures/diagramme}
\end{center}

\end{document}
