\documentclass{tnreport1}
%\documentclass[stage2a]{tnreport1} % If you are in 2nd year
%\documentclass[confidential]{tnreport1} % If you are writing confidential report

\def\reportTitle{Évaluation d'une nouvelle solution de contrôle d'accès\\mise sur le marché par l'entreprise Super Sûr} % Titre du mémoire
\def\reportAuthor{Ophélien Amsler \& Bertrand Müller}

\usepackage{lipsum}
\usepackage{subcaption}
\usepackage{adjustbox}
\usepackage{dirtree}
\usepackage{tikz}
\usepackage{multirow}
\usetikzlibrary{trees}
\usepackage{tikz-qtree}
\usepackage{makecell}
\usepackage{hhline}
\usepackage{colortbl}
\usepackage[most]{tcolorbox}
\usepackage{pdfpages}
\usepackage{draftwatermark}
\usepackage[stable]{footmisc}
\usepackage{hhline}
\usepackage{float}
\usepackage{graphicx}
\usepackage{enumitem}

\begin{document}

\maketitle

\clearpage

\renewcommand{\baselinestretch}{0.5}\normalsize
\tableofcontents
\renewcommand{\baselinestretch}{1.0}\normalsize

\clearpage

\chapter{Introduction}

En tant que spécialistes pour l'évaluation de systèmes de contrôle d'accès, notre collectif Incognito souhaite analyser un produit nouvellement mis sur le marché et basé sur l'utilisation de badges. Nous avons donc élaboré un document permettant d'évaluer ce nouveau produit, principalement d'un point de vue cryptographique. 

Afin de mettre l'accent sur ce sujet, une première partie présente l'architecture globale du système, une seconde réalise une cryptanalyse du produit et une dernière concerne l'analyse des risques. A la fin de ce rapport, notre collectif donne son avis global sur cette nouvelle solution en présentant ses points forts et faibles. 

Ce rapport vise donc à présenter, de manière indépendante, la sécurisation mise en place pour ce produit et éventuellement alerter l'entreprise Super Sûr sur d'éventuelles failles. 

\clearpage

\chapter{Architecture globale}

Dans cette première partie, on s'intéresse à l'architecture globale du système de contrôle d'accès en se basant sur le document de spécifications élaboré par l'entreprise Super Sûr. 

\section{Besoins visés}

Tout d'abord, on souhaite décrire les besoins auxquels ce système répond afin de cerner les enjeux de sécurisation d'une telle solution. Pour cela, on prend l'exemple du laboratoire TheLightUniverse dans le cadre de son échange avec l'entreprise Super Sûr. La solution consiste à mettre en place un système de contrôle d'accès par badge pour sécuriser les différentes zones d'un bâtiment. Elle prévoit donc l'utilisation de lecteurs de badges pour déterminer les privilèges accordés à un détenteur de badge. En fonction des droits qui lui sont accordés, l'accès lui est accordé ou non selon le type de salle.

A la vue du document de spécifications, l'architecture globale semble répondre aux besoins exposés par le laboratoire. En effet, le projet prévoit de :

\begin{enumerate}
	\item 	\textbf{Point de vue technique}
			\begin{itemize}
				\item \underline{Gérer différents niveaux d'accès.} Ceci est effectivement l'un des points les plus importants dans la réalisation de ce projet. Il est clairement exposé et présenté dans le document rédigé par l'entreprise.
				
				\item \underline{Gérer différentes catégories d'utilisateur.} Cet aspect est bien pensé puisque cela permet d'affecter un privilège à un ensemble de salariés possédant un rôle identique. 
				
				\item \underline{Utiliser des cartes à puce avec code PIN personnel.} Un salarié est alors amené à insérer sa carte dans le lecteur et à entrer le code PIN correspondant pour accéder à l'espace souhaité. De plus, la position des chiffres sur le lecteur est aléatoire afin d'éviter de repérer les chiffres fréquemment utilisés. 
				
				\item \underline{Définir un protocole lors de la perte d'un badge.} Un signalement pourra être fait par le propriétaire d'un badge en vue de le désactiver. Cette action peut être réalisée depuis l'intranet du laboratoire pour le désactiver immédiatement. 
							
				\item \underline{Utiliser un réseau dédié.} Pour gérer l'ouverture des portes, le serveur et l'ensemble des lecteurs de badges communiquent via un réseau dédié. De cette manière, il est alors impossible d'utiliser internet pour accéder au système de sécurité interne.
				
				\item \underline{Mettre en place une interface d'administration.} En effet, l'entreprise souhaite donner libre cours au laboratoire d'ajouter des badges, d'en supprimer, de modifier les privilèges, d'ajouter des salles, ... Ceci est un très bon point pour faciliter la prise en main de la solution et d'éviter au client de demander des modifications régulières auprès de Super Sûr. 
				
				\item \underline{Gérer les cas de crise.} Un autre point intéressant de la solution correspond à la gestion des cas exceptionnels pouvant survenir. Lors d'une panne du serveur central, un administrateur peut suivre un protocole pour agir directement sur le système. Par ailleurs, la solution est sensible à d'autres événements comme une panne électrique, un incendie... 
			\end{itemize}
		
	\medskip
	
	\item 	\textbf{Point de vue technologique}
	\begin{itemize}
		\item \underline{Être novatrice.} Ceci répond complètement aux tendances du marché. Afin qu'il soit plus sûr, le système repose sur de nouvelles technologies pour contrôler les accès par badge.
	\end{itemize}
		
	\medskip
		
	\item 	\textbf{Point de vue financier}
			\begin{itemize}
				\item \underline{Représenter un faible coût.} Le dernier avantage de cette solution correspond aux faibles frais de la mise en place du système de contrôle d'accès.
			\end{itemize}
\end{enumerate}

Au niveau de son architecture globale, le projet correspond aux tendances actuelles pour la mise en place d'un système de contrôle d'accès par badge. En effet, il répond à l'ensemble des critères exposés précédemment, ce qui semble en faire un produit de choix. 

\section{Omissions}

Malgré une très bonne solution qui répond aux besoins, notre analyse nous a permis d'identifier quelques oublis. Cependant, nous n'affirmons pas que les points abordés par la suite n'ont pas été pris en compte par Super Sûr pour la solution exposée. Ils n'ont simplement pas été retranscrits dans le document.

Lors de l'analyse des besoins, il aurait été judicieux d'indiquer si :

\begin{itemize}
	\item Un système d'alarme est prévu lorsqu'une intrusion par la force physique est détectée. En d'autres termes, est-ce que les administrateurs ont la possibilité d'avoir connaissance des tentatives réalisées sur une période donnée ? A l'heure actuelle, cela ne semble pas être le cas. 
	
	\medskip
	
	\item Un système de base de données a été privilégié par rapport à ce système de contrôle d'accès. Un extrait du système de bases de données nous aurait permis d'affirmer ou d'infirmer que le besoin de sécurité est respecté de bout en bout. 
	
	\medskip
	
	\item Des mesures de performance ont été réalisées sur le serveur, les lecteurs et le réseau dédié. Le laboratoire a sûrement exposé des contraintes de temps de réponse pour accorder ou verrouiller l'accès à une pièce. 
\end{itemize}

\clearpage

\chapter{Cryptanalyse}

Nous nous sommes ensuite intéressés aux moyens de cryptographie mis en œuvre. 

\section{Solution cryptographique}

La solution propose l'utilisation de cartes à puce à identifiant unique et avec code PIN personnel (de type "carte de crédit"). En réalité, lors de l'authentification, ce code PIN est crypté grâce à une clé privée gravée sur 4096 bits. Cela repose donc sur la technologie RSA correspondant à une assurance de sécurité, ce qui est un très bon point pour la solution. De plus, la technologie RSA correspond à l'un des plus hauts niveaux de sécurité pour les applications basées sur des outils cryptographiques. Elle est encore recommandée pour plusieurs années. Super Sûr répond donc aux attentes du client avec une solution viable pour les prochaines années. 

Une fois que le cryptage a été réalisé, le code PIN et l'identifiant sont récupérés par le lecteur de badge au moment de l'authentification. Ces informations sont alors redirigées vers le serveur central en étant cryptées une nouvelle fois, grâce au protocole de sécurisation des échanges \textit{Secure Sockets Layer} (SSL). 

La dernière étape est réalisée au niveau du serveur central lorsqu'il reçoit la demande d'accès. Les données sont alors décryptées une première fois puis une seconde en utilisant la clé publique du badge, retrouvée sur le système de stockage des données grâce à l'identifiant. Le serveur renvoie alors sa réponse pour permettre ou non l'accès à la salle.

\section{Analyse}

Nous souhaitons désormais effectuer une analyse critique de la solution cryptographique proposée et présenter nos recommandations. 

\subsection{Badge}

En ce qui concerne le badge, il n'est pas indiqué quel type de carte à puce sera utilisée. Nous ne nous étendrons donc pas davantage sur ce point. Cependant, étant donné que la clé RSA privée et le code PIN personnel sont stockés sur le badge, un risque peut se présenter. En effet, il se peut que quelqu'un décide d'accéder à ces données et donc de connaître le code secret pour accéder aux salles. L'authentification serait alors possible pour n'importe quelle personne arrivant à récupérer ces informations.

Toute personne possédant un badge doit donc veiller à ne pas le perdre pour éviter la copie des données lisibles sur le badge. Il serait alors possible pour un hacker de faire croire à n'importe quel terminal que le détenteur du badge est bien le bon. A l'instar des cartes de crédit, si les badges mis en circulation sont de bonne qualité, la sécurité est toutefois suffisante puisqu'il faut du matériel professionnel pour exploiter ces vulnérabilités. De plus, les attaques sur les cartes de crédit sont relativement ardues et restent rares (la recopie des données est complexe).

\subsection{Transmission}

En revanche, la transmission du code PIN et de l'identifiant du badge sur le réseau est fortement sécurisée et ne présente pas de risque majeur. En effet, les données sont cryptées grâce à une clé RSA sur 4096 bits. Par la suite, ces données sont de nouveau cryptées grâce à SSL. Quelqu'un d'étranger au service d'administration des badges aura alors de grandes difficultés pour recueillir l'identifiant et le code PIN associé à un badge en cours d'utilisation. De plus, à aucun moment, la clé RSA privée n'est transmise via le réseau. La sécurité est donc garantie entre le lecteur de badge et le serveur central (et inversement).

La solution présente donc un énorme avantage à ce niveau : cryptage conséquent avec une transmission sur un réseau dédié et non relié à internet. Cela va donc en faveur de l'installation du système de l'entreprise Super Sûr. 

De plus, le choix de RSA est bon car il s'agit d'un des systèmes de cryptage les plus sûrs actuellement. Il s'agit d'un algorithme de chiffrement asymétrique, c'est-à-dire qu'il existe une distinction entre des données publiques et privées pour garantir la confidentialité. La clé privée n'étant connue que du badge et la clé publique que du serveur, toute personne sera incapable de déchiffrer le code PIN associé à un badge. Il faudrait un grand nombre d'années pour calculer exhaustivement toutes les combinaisons possibles avec 4096 bits. Cela assure donc un niveau de sécurité suffisant pour plusieurs années. 

\subsection{Serveur central}

Enfin, du côté du serveur central, les données reçues sont cryptées. Il se charge donc de les décrypter sachant qu'il connaît la clé publique d'un badge donné. A ce niveau, peu de failles peuvent se manifester puisque le serveur est sécurisé par des pare-feux, des antivirus etc... et se trouve sur un réseau dédié ! 

En dehors de tout aspect cryptographique, la mise en garde de notre collectif réside dans la nomination des administrateurs ayant accès au serveur. Ces personnes devront absolument être de confiance et se charger de modifier les mots de passe, de mettre à jour les services de sécurité du serveur et de consulter régulièrement l'interface d'administration afin de garantir une sécurité permanente du système de contrôle d'accès. Si jamais une personne étrangère à ce service a la possibilité d'accéder au serveur, l'intégrité du système serait alors fortement altérée. 

\clearpage

\chapter{Analyse des risques}

Dans cette nouvelle partie, le collectif Incognito a décidé de présenter différents scénarios pour lesquels un risque de sécurité se présente. Dans ce chapitre, on s'intéresse davantage aux actions humaines plutôt qu'à des aspects techniques. 

\section{Vol/Perte de badge}

L'un des premiers risques concerne le vol ou la perte de badge. Si un employé égare son badge, on peut potentiellement envisager qu'une autre personne soit en mesure de l'utiliser pour son propre compte. Si ce badge n'a pas été déclaré comme volé ou perdu auprès des administrateurs, le badge reste actif et est normalement utilisable. Ainsi, les salles sont accessibles au nouveau détenteur du badge. Cette nouvelle personne pourrait alors causer des actions nuisibles et des dommages importants à l'entreprise. 

\section{Déclenchement d'alarme}

Lors du déclenchement de l'alarme incendie par exemple, le système prévoit d'ouvrir toutes les portes pour des raisons de sécurité et pour respecter les normes à ce sujet. Cependant, cela présente tout de même un risque car elle peut être déclenchée volontairement par la création de fumée ou l'utilisation d'objets chauds auprès des alarmes de détection d'incendie. 

Si le déclenchement de l'alarme s'avère volontaire (ou non !), tous les accès seront ouverts et des personnes pourraient notamment tenter d'accéder au serveur central contenant les bases de données du système de contrôle d'accès. L'altération des données est alors possible puisqu'aucune mesure de cryptage spécifique ne leur est dédiée. Toutefois, la personne s'introduisant dans le serveur devra connaître les techniques de piratage des accès. 

\section{Code PIN écrit sur papier}

Une autre erreur humaine peut consister à écrire son code PIN sur un papier et à le déposer sur son bureau ou dans ses affaires (tel qu'un portefeuille). Si cette personne dispose d'accès aux salles de plus haute sécurité, des actions nuisibles pourraient également être entreprises à l'encontre de l'entreprise. Cependant, ce risque reste limité car il est nécessaire d'être en possession du badge pour lequel on connaît le code secret. Selon toute hypothèse, la personne essayant d'entreprendre de telles actions serait rapidement confondue. 

\chapter{Points forts \& faibles}

Enfin, nous avons listé les points forts et les points faibles de la solution examinée. Nous en présentons donc une liste avec une description pour chacun d'eux. 

\subsection*{Points forts}

\begin{itemize}
	\item \underline{Interface d'administration.} La présence d'une interface d'administration est un très bon point quant à la solution. En effet, cette dernière permet de configurer le système de contrôle d'accès et de le contrôler à plusieurs niveaux. 
	
	\item \underline{Utilisation de badges à puce.} L'utilisation de ce type de badge permet d'avoir une sécurité renforcée et de stocker un code PIN unique et secret pour chacun des salariés. 
	
	\item \underline{Lecteur de badge intelligent.} Un autre point essentiel de cette solution concerne la lecture des badges et la sécurisation du système de code PIN associé. Grâce à l'intelligence des lecteurs, la position des chiffres est aléatoire et ne permet donc pas d'en déduire ceux les plus couramment utilisés. De plus, le nombre de tentatives est limité pour éviter les intrusions par force brute. 
	
	\item \underline{Réseau dédié.} Le système de contrôle d'accès est basé sur le serveur central. Or, celui-ci n'est pas connecté à internet. Il ne peut donc pas être attaqué de l'extérieur. 
	
	\item \underline{Protocoles cryptographiques.} Les méthodes de cryptographie utilisées entre le badge et le serveur central sont maitrisées de bout en bout et correspondent à des standards. La sécurité est donc renforcée et prouvée. 
	
	\item \underline{Anonymisation.} Les badges ne contiennent pas des informations sur leur détenteur. Ainsi, en cas de perte ou de vol, il est impossible pour une personne malveillante de connaître l'identité du propriétaire du badge. 
\end{itemize}

\subsection*{Points faibles}

\begin{itemize}
	\item \underline{Alarmes.} A ce jour, aucun système d'alarme n'est prévu pour la solution. Ainsi, si des lecteurs sont arrachés, il sera alors impossible de détecter de tels agissements (à l'exception d'une détection visuelle). 
	
	\item \underline{Évaluation des performances.} Par ailleurs, aucun test ou aucune mesure de performance n'ont été menés. Il est alors difficile de déterminer l'efficacité et la fiabilité de la solution. 
	
	\item \underline{Cryptage du serveur.} Le serveur local n'est pas crypté. Il est alors facile pour une personne malveillante d'en faire une copie pour récupérer les informations stockées. Cela s'étend également aux bases de données qui ne semblent pas hacher les informations sensibles telles que le code PIN. 
	
	\item \underline{Sauvegardes.} Aucune sauvegarde du serveur central ou de la base de données n'est prévue. Ainsi, si les données sont corrompues, il est impossible de les restaurer et de revenir à un état antérieur fonctionnel. Dans le pire des cas, les données peuvent être complètement perdues. La tâche consiste donc à enregistrer de nouveau tous les utilisateurs et leur badge.
\end{itemize}

\clearpage

\chapter{Conclusion}

En conclusion, nous sommes prêts à acheter cette solution et à la promouvoir. Les dispositifs de sécurité employés par l'entreprise Super Sûr sont efficaces et judicieux et assurent la fiabilité du produit dans son ensemble. 

Cependant, quelques faiblesses sont à prendre en compte si vous souhaitez vous orienter vers ce système de contrôle d'accès. En effet, quelques points restent à éclaircir : comment les codes PIN sont transmis aux employés lors de la création du badge ? Quelle est la taille du code PIN ? De plus, il pourrait être intéressant de former le personnel aux risques de sécurité quant à leurs agissements. Le niveau de sûreté d'un système est également évalué sur sa résistance aux erreurs humaines. 

Enfin, le coût de la mise en place est inconnu. Il est donc ardu de comparer cette solution aux autres du marché et de réaliser un comparatif. Elle est toutefois complète et répond aux besoins exposés par le client. 

\end{document}
